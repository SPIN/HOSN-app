import { Component } from "@angular/core";
import {
  NavController,
  ToastController,
  LoadingController,
  AlertController
} from "ionic-angular";
import { Storage } from "@ionic/storage";
import { CryptoProvider } from "../../providers/crypto/crypto";
import { SocialSharing } from "@ionic-native/social-sharing";

@Component({
  selector: "page-settings",
  templateUrl: "settings.html"
})
export class SettingsPage {
  keywords: string;
  privateKey: string;
  publicKey: string;

  constructor(
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    private cryptoUtils: CryptoProvider,
    private storage: Storage,
    private loadingCtrl: LoadingController,
    private sharing: SocialSharing,
    private alertCtrl: AlertController
  ) {
    this.loadValuesFromStorage();
  }

  async loadValuesFromStorage() {
    this.privateKey = await this.storage.get("privateKey");
    this.publicKey = await this.storage.get("publicKey");
    this.keywords = await this.storage.get("keywords");
  }

  generateKeys() {
    if (this.publicKey || this.privateKey) {
      const alert = this.alertCtrl.create({
        title: "Are you sure?",
        subTitle:
          "You already have keys entered. Do you want to overwrite them?",
        buttons: [
          {
            text: "No",
            role: "cancel"
          },
          {
            text: "Yes",
            handler: () => {
              this.startKeyGeneration();
            }
          }
        ]
      });

      alert.present();
    } else {
      this.startKeyGeneration();
    }
  }

  private async startKeyGeneration() {
    const keys = await this.cryptoUtils.generateRsaKeys();

    this.publicKey = await this.cryptoUtils.extractPublicKey(keys);
    this.privateKey = await this.cryptoUtils.extractPrivateKey(keys);
  }

  save() {
    this.storage.set("publicKey", this.publicKey);
    this.storage.set("privateKey", this.privateKey);
    this.storage.set("keywords", this.keywords ? this.keywords.trim() : "");

    this.showToast("Successfully saved!");
  }

  async publishPublicKey() {
    if (this.publicKey.length) {
      const loading = this.loadingCtrl.create();
      loading.present();

      await this.cryptoUtils.publishPublicKey(this.publicKey);

      loading.dismiss();
      this.showToast("Public key has been published!");
    }
  }

  exportPrivateKey() {
    if (this.privateKey.length) {
      this.sharing
        .share(this.privateKey, null, null, null)
        .then(() => console.log("Private key was exported"))
        .catch(() =>
          this.showToast(
            "Sorry! Something went wrong trying to export the private key :("
          )
        );
    } else {
      this.showToast("There is nothing to share.");
    }
  }

  private showToast(message: string) {
    const toast = this.toastCtrl.create({
      message: message,
      position: "bottom",
      duration: 3000
    });
    toast.present();
  }
}
