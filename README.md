# HOSN - Hybrid Online Social Network
_"Tweet beyond the cage of commercial  social networks"_

This repository contains an Android app, as a proof of concept for HOSN, builds on top of Twitter and provides users with additional means of privacy control.

## Synopsis
The idea of HOSNs is to combine a commercial online social network (COSN) and a privacy-preserving online social network (PPOSN) . 
That combination enables to utilize both the market penetration of COSNs and the anonymity of PPOSNs. 
The COSN serves as base network and provides the user base as well as the connectivity between these users. 
The PPOSN is established above, providing the users with means of communication beyond the knowledge of the provider of the COSN.
Simply put, the objective of HOSN is providing the users of COSNs with additional means of privacy control by establishing a PPOSN on top of the COSN.

### References
[1] [Wainakh et al. (2019) Tweet beyond the Cage: A Hybrid Solution for the Privacy Dilemma in Online Social Networks](https://ieeexplore.ieee.org/document/9013901)

[2] [RTG Privacy and Trust for Mobile Users - Privacy Protection via Hybrid Social Networks](https://www.informatik.tu-darmstadt.de/privacy-trust/research_5/research_area_b__privacy_and_trust_in_social_networksresearch_area_b__privacy_and_trust_in_social_networks/b__2_privacy_protection_via_hybrid_social_networks/index.en.jsp)

## Authors

- __Aidmar Wainakh__ - _concept development, guidance and suggestions during development_

- __Jörg Daubert__ - _concept development, guidance and suggestions during development_

- __Tim Grube__ - _concept development_

- __Carsten Porth__ - _development of the first prototype within his Master Thesis_

- __Rohit Gowda__ - _enhancements within his Master Thesis_

## Contact
Aidmar Wainakh (<wainakh@tk.tu-darmstadt.de>)